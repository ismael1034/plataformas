﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckGround : MonoBehaviour
{

    private Player playercontrol;
    private bool muerto = false;
  


    // Use this for initialization
    void Start()
    {

        playercontrol = GetComponentInParent<Player>();

    }

    // Update is called once per frame

    void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag == "Ground")
        {


            playercontrol.grounded = true;

        }

        if (col.gameObject.tag == "Platfourm")
        {


            playercontrol.grounded = true;

        }

    }

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "Ground")
        {
            playercontrol.grounded = false;
        }

        if (col.gameObject.tag == "Platfourm")
        {
            playercontrol.grounded = false;
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "muerte")
        {
           
            MyGameManager.getInstance().Loselife();
        }
    }
}
