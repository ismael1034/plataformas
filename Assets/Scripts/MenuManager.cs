﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    
    public void PlayGame()
    {
        SceneManager.LoadScene("Gameplay");
    }

    public void Controls()
    {
        SceneManager.LoadScene("Controles");
    }

    public void EndGame()
    {

        Application.Quit();
    }
}
