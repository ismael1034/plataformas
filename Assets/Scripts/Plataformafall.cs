﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataformafall : MonoBehaviour {

    public float fallDelay = 0.0f;
    public float respawnDelay = 0.0f;

    private Rigidbody2D rb2d;
    private BoxCollider2D bcollider;
    private Vector3 start;

	// Use this for initialization
	void Start () {

        rb2d = GetComponent<Rigidbody2D>();
        bcollider = GetComponent<BoxCollider2D>();
        start = transform.position;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("Player"))
        {
            Invoke("Fall", fallDelay);
            Invoke("Respawn", fallDelay + respawnDelay);
        }
    }

    void Fall()
    {
        rb2d.isKinematic = false;
        bcollider.isTrigger = true;
    }

    void Respawn()
    {
        transform.position = start;
        rb2d.isKinematic = true;
        rb2d.velocity = Vector3.zero;
        bcollider.isTrigger = false;
    }
}
