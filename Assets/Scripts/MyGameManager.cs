﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MyGameManager : MonoBehaviour

{

    public GameObject vidas;
    private int lives;
    public Text livesText;
    private static MyGameManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

    }

        public static MyGameManager getInstance()
    {
        return instance;
    }

    // Use this for initialization
    void Start()
    {

        lives = 3;
        livesText.text = "x " + lives.ToString();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Loselife()
    {
        if (lives < 1)
        {
            SceneManager.LoadScene("Lose");
        }
        else
        {
            lives--;
            livesText.text = "x " + lives.ToString();
        }
    }
   
}
