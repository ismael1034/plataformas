﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    // Variables publicas para poder modifircar desde el inspector
    public float maxS = 11f;
    //Variables privadas
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    private Animator anim;
    private bool flipped = false;
    private bool muerto = false;
    private Vector3 origen;
    public float jumpPower = 6.5f;
    private bool Jump;
    public bool grounded;
    // sonidos 
    public AudioSource salto;
    public AudioSource lose;

    // Use this for initialization
    void Awake()
    {
        origen = transform.position;
        // Obtenemos el rigidbody y lo guardamos en la variable rb2d
        // para poder utilizarla más cómodamente
        rb2d = GetComponent<Rigidbody2D>();
        // Obtenemos el Animator Controller para poder modificar sus variables
        anim = GetComponent<Animator>();
    }

   

    private void Update()
    {


        if (Input.GetKeyDown(KeyCode.UpArrow) && grounded)
        {
            anim.SetBool("Jump", true);
            Jump = true;
            salto.Play();
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //Miramos el input Horizontal
        move = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);

        if (Jump)
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
            rb2d.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
            Jump = false;
            anim.SetBool("Jump", false);
        }

        //Miramos si nos estamos moviendo.
        // OJO!! Nunca comparar con 0 floats, nunca será 0 perfecto, siempre hay un error de redondeo
        if (rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f)
        {
            if ((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped))
            {
                flipped = !flipped;
                this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
            }
            anim.SetBool("Walk", true);
        }
        else
        {
            anim.SetBool("Walk", false);
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "muerte")
        {
            muerto = true;
            anim.SetBool("Die", true);
            lose.Play();
        
         
        }
        
        if (coll.gameObject.tag == "win")
        {
            SceneManager.LoadScene("Win");
        }
    }


   /* private void OnBecameInvisible()
    {
        this.transform.position = origen;
    }
    */
    void Restart()
    {
        this.transform.position = origen;
        anim.SetBool("Die", false);
        muerto = false;
        anim.Play("Idle");
    }

}